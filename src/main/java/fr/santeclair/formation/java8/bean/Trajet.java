package fr.santeclair.formation.java8.bean;

import java.util.Optional;

import fr.santeclair.formation.java8.util.DistanceUtil;

public class Trajet {
	private int id;
	private Coords debut;
	private Coords fin;
	private int stepDebut;
	private int stepFin;
	private Voiture voitureAffecte;

	public Trajet(int id, Coords debut, Coords fin, int stepDebut, int stepFin) {
		this.id = id;
		this.debut = debut;
		this.fin = fin;
		this.stepDebut = stepDebut;
		this.stepFin = stepFin;
	}

	public int getDistance() {
		return DistanceUtil.getDistance(debut, fin);
	}

	public void affecterVoiture(Voiture voiture) {
		this.voitureAffecte = voiture;
	}

	public Optional<Voiture> getVoitureAffecte() {
		return Optional.ofNullable(this.voitureAffecte);
	}

	/**
	 * @return the debut
	 */
	public Coords getDebut() {
		return debut;
	}

	/**
	 * @return the fin
	 */
	public Coords getFin() {
		return fin;
	}

	/**
	 * @return the stepDebut
	 */
	public int getStepDebut() {
		return stepDebut;
	}

	/**
	 * @return the stepFin
	 */
	public int getStepFin() {
		return stepFin;
	}

	public int getId() {
		return id;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Trajet other = (Trajet) obj;
		if (debut == null) {
			if (other.debut != null) {
				return false;
			}
		} else if (!debut.equals(other.debut)) {
			return false;
		}
		if (fin == null) {
			if (other.fin != null) {
				return false;
			}
		} else if (!fin.equals(other.fin)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		if (stepDebut != other.stepDebut) {
			return false;
		}
		if (stepFin != other.stepFin) {
			return false;
		}
		return true;
	}

}
