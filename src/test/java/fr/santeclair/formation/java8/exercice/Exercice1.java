package fr.santeclair.formation.java8.exercice;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.util.Date;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercice1 {
	/*
	 * LocalDate
	 */
	/**
	 * Créez un nouvel objet {@link LocalDate} au 2 avril 2018
	 */
	private LocalDate exercice01CreateDate() {
		return null;
	}

	/**
	 * Parsez la date en paramètre au format dd/MM/yyyy
	 */
	private LocalDate exercice02ParseDate(String formatedDate) {
		return null;
	}

	/**
	 * Ajoutez une semaine à la date en paramètre.
	 */
	private LocalDate exercice03ChangeDate(LocalDate date) {
		return null;
	}

	/**
	 * Formattez la date de la manière suivantes :<br>
	 * MM/dd/yy
	 */
	private String exercice04FormatDate(LocalDate date) {
		return null;
	}

	/*
	 * LocalDateTime
	 */
	/**
	 * Créez un nouvel objet {@link LocalDateTime} au 31 janvier 2015 à 13h37
	 */
	private LocalDateTime exercice05CreateDateTime() {
		return null;
	}

	/**
	 * Parsez la date en paramètre au format dd/MM/yyyy HH:mm:ss
	 */
	private LocalDateTime exercice06ParseDateTime(String formatedDate) {
		return null;
	}

	/**
	 * Soustrayez deux années à la date en paramètre.
	 */
	private LocalDateTime exercice07ChangeDateTime(LocalDateTime date) {
		return null;
	}

	/**
	 * Formattez la date de la manière suivante :<br>
	 * 'Le' d MMMM yyyy 'à' H 'heures' m 'minutes et' s 'secondes'
	 */
	private String exercice08FormatDateTime(LocalDateTime date) {
		return null;
	}

	/*
	 * LocalTime
	 */
	/**
	 * Créez un nouvel objet {@link LocalTime} à 12:13:14
	 */
	private LocalTime exercice09CreateTime() {
		return null;
	}

	/**
	 * Parsez l'horaire en paramètre au format HH:mm:ss
	 */
	private LocalTime exercice10ParseTime(String formatedTime) {
		return null;
	}

	/**
	 * Ajoutez 49 minutes à l'horaire en paramètre.
	 */
	private LocalTime exercice11ChangeTime(LocalTime time) {
		return null;
	}

	/**
	 * Formatter l'horaire de la manière suivante :<br>
	 * hh mm a
	 */
	private String exercice12FormatTime(LocalTime date) {
		return null;
	}

	/*
	 * Compatibilité avec Date
	 */
	/**
	 * Transformer l'objet {@link Date} en {@link LocalDateTime}.<br>
	 * Attention à la zone française (GMT+1 appelé aussi CET ou ECT).
	 */
	private LocalDateTime exercice13DateToLocalDateTime(Date date) {
		return null;
	}

	/**
	 * Transformez l'objet {@link LocalDateTime} en {@link Date}.<br>
	 * Attention à la zone française (GMT+1 appelé aussi CET ou ECT).
	 */
	private Date exercice14LocalDateTimeToDate(LocalDateTime dateTime) {
		return null;
	}

	/*
	 * Vérification des exercices
	 */
	@Test
	public void testExercice01CreateDate() {
		LocalDate date = exercice01CreateDate();
		Assert.assertEquals("2018-04-02", date.toString());
	}

	@Test
	public void testExercice02ParseDate() {
		String formatedDate = "30/03/2017";
		LocalDate date = exercice02ParseDate(formatedDate);
		Assert.assertEquals("2017-03-30", date.toString());
	}

	@Test
	public void testExercice03ChangeDate() {
		LocalDate date = LocalDate.of(2015, Month.JULY, 14);
		LocalDate datePlus7 = exercice03ChangeDate(date);
		Assert.assertEquals("2015-07-21", datePlus7.toString());
	}

	@Test
	public void testExercice04FormatDate() {
		LocalDate date = LocalDate.of(2019, Month.APRIL, 20);
		String sDate = exercice04FormatDate(date);
		Assert.assertEquals("04/20/19", sDate);
	}

	@Test
	public void testExercice05CreateDateTime() {
		LocalDateTime date = exercice05CreateDateTime();
		Assert.assertEquals("2015-01-31T13:37", date.toString());
	}

	@Test
	public void testExercice06ParseDateTime() {
		String formatedDateTime = "30/03/2017 23:34:45";
		LocalDateTime date = exercice06ParseDateTime(formatedDateTime);
		Assert.assertEquals("2017-03-30T23:34:45", date.toString());
	}

	@Test
	public void testExercice07ChangeDateTime() {
		LocalDateTime date = LocalDateTime.of(2018, Month.SEPTEMBER, 2, 20, 0);
		LocalDateTime dateMinus1Year = exercice07ChangeDateTime(date);
		Assert.assertEquals("2016-09-02T20:00", dateMinus1Year.toString());
	}

	@Test
	public void testExercice08FormatDateTime() {
		LocalDateTime date = LocalDateTime.of(2018, Month.APRIL, 7, 9, 28, 47);
		String sDate = exercice08FormatDateTime(date);
		Assert.assertEquals("Le 7 avril 2018 à 9 heures 28 minutes et 47 secondes", sDate);
	}

	@Test
	public void testExercice09CreateTime() {
		LocalTime time = exercice09CreateTime();
		Assert.assertEquals("12:13:14", time.toString());
	}

	@Test
	public void testExercice10ParseTime() {
		String formatedTime = "08:40:15";
		LocalTime time = exercice10ParseTime(formatedTime);
		Assert.assertEquals("08:40:15", time.toString());
	}

	@Test
	public void testExercice11ChangeTime() {
		LocalTime time = LocalTime.of(20, 10, 30);
		LocalTime newTime = exercice11ChangeTime(time);
		Assert.assertEquals("20:59:30", newTime.toString());
	}

	@Test
	public void testExercice12FormatTime() {
		LocalTime date = LocalTime.of(13, 30, 55);
		String sDate = exercice12FormatTime(date);
		Assert.assertEquals("01 30 PM", sDate);
	}

	@Test
	public void testExercice13DateToLocalDateTime() {
		Date date = new Date(1_486_979_700_000L);
		LocalDateTime dateTime = exercice13DateToLocalDateTime(date);
		Assert.assertEquals("2017-02-13T10:55", dateTime.toString());
	}

	@Test
	public void testExercice14LocalDateTimeToDate() throws ParseException {
		LocalDateTime dateTime = LocalDateTime.of(2018, Month.SEPTEMBER, 30, 23, 49, 11);
		Date date = exercice14LocalDateTimeToDate(dateTime);
		Assert.assertEquals(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss zzz").parse("2018-09-30 23:49:11 CET"), date);
	}

}
