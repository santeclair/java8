package fr.santeclair.formation.java8.util;

import java.util.Map.Entry;

public class EntryUtil {
	public static <R> R getValue(Entry<?, R> entry) {
		if (entry == null) {
			return null;
		}

		return entry.getValue();
	}
}
