package fr.santeclair.formation.java8.bean;

public class Score {
	private int nbTrajet = 0;
	private int nbTrajetTraite = 0;
	private int nbTrajetReussi = 0;
	private int nbBonusTrajet = 0;
	private int score = 0;

	public Score(int nbTrajet) {
		this.nbTrajet = nbTrajet;
	}

	public void traiterEtatVoiture(EtatVoiture etat) {
		this.nbTrajetTraite += etat.getNbTrajet();
		this.nbBonusTrajet += etat.getNbBonus();
		this.nbTrajetReussi += etat.getNbCourseReussi();
		this.score += etat.getScore();
	}

	public void fusionner(Score score) {
		this.nbTrajetTraite += score.nbTrajetTraite;
		this.nbBonusTrajet += score.nbBonusTrajet;
		this.nbTrajetReussi += score.nbTrajetReussi;
		this.score += score.score;
	}

	public int getNbTrajet() {
		return nbTrajet;
	}

	public int getScore() {
		return score;
	}

	public int getNbTrajetTraite() {
		return nbTrajetTraite;
	}

	public int getNbTrajetReussi() {
		return nbTrajetReussi;
	}

	public int getNbBonusTrajet() {
		return nbBonusTrajet;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Nb trajets traités : ").append(nbTrajetTraite).append(" (").append(nbTrajetTraite * 100 / nbTrajet)
				.append("%)\n");
		sb.append("Nb trajets réussis : ").append(nbTrajetReussi).append(" (")
				.append(nbTrajetReussi * 100 / nbTrajetTraite).append("%)\n");
		sb.append("Nb trajets avec bonus : ").append(nbBonusTrajet).append(" (")
				.append(nbBonusTrajet * 100 / nbTrajetReussi).append("%)\n");
		sb.append("Score : ").append(score).append("\n");
		return sb.toString();
	}
}
