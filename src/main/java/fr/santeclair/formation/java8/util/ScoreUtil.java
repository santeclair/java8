package fr.santeclair.formation.java8.util;

import java.util.List;
import java.util.stream.Collectors;

import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Score;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;

public class ScoreUtil {
	public static Score scorePlanning(Planning planning) {
		if (hasTrajetDoublon(planning.getVoitures())) {
			throw new IllegalArgumentException("Des trajets sont en doublon");
		}
		Score score = planning.getVoitures().parallelStream().map(t -> t.score()).collect(
				() -> new Score(planning.getTrajets().size()), (s, e) -> s.traiterEtatVoiture(e),
				(a, s) -> a.fusionner(s));
		System.out.println(score.toString());
		return score;
	}

	public static boolean hasTrajetDoublon(List<Voiture> voitures) {
		return voitures.stream().flatMap(t -> t.getTrajets().stream())
				.collect(Collectors.groupingBy(Trajet::getId, Collectors.counting())).entrySet().stream()
				.allMatch(e -> e.getValue() > 1);
	}

}