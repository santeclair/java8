package fr.santeclair.formation.java8.bean;

import fr.santeclair.formation.java8.util.DistanceUtil;

public class EtatVoiture {
	private Coords position;
	private int temps;
	private int score = 0;
	private int nbTrajet = 0;
	private int nbBonus = 0;
	private int nbCourseReussi = 0;
	private final int bonusPrisePassager;
	private final int tempsLimite;

	public EtatVoiture(int bonusPrisePassager, int tempsLimite) {
		this.position = new Coords(0, 0);
		this.temps = 0;
		this.score = 0;
		this.bonusPrisePassager = bonusPrisePassager;
		this.tempsLimite = tempsLimite;
	}

	public int estimerGainTrajet(Trajet trajet) {
		int tempsTemp = calculTempsAuDemarrageTrajet(trajet);
		int bonus = calculBonusTrajet(trajet, tempsTemp);
		return (aReussiLaCourse(trajet, tempsTemp) ? bonus + trajet.getDistance() : -100000) - (tempsTemp - this.temps);
	}

	public void faireTrajet(Trajet trajet) {
		int tempsTemp = calculTempsAuDemarrageTrajet(trajet);
		int bonus = calculBonusTrajet(trajet, tempsTemp);
		this.nbTrajet++;
		this.temps = tempsTemp + trajet.getDistance();
		this.position = trajet.getFin();
		if (aReussiLaCourse(trajet, tempsTemp)) {
			this.score += bonus + trajet.getDistance();
			this.nbBonus++;
			this.nbCourseReussi++;
		}
	}

	private int calculTempsAuDemarrageTrajet(Trajet trajet) {
		return Math.max(this.temps + DistanceUtil.getDistance(position, trajet.getDebut()), trajet.getStepDebut());
	}

	private int calculBonusTrajet(Trajet trajet, int tempsTemp) {
		return tempsTemp == trajet.getStepDebut() ? this.bonusPrisePassager : 0;
	}

	private boolean aReussiLaCourse(Trajet trajet, int tempsTemp) {
		return (tempsTemp + trajet.getDistance()) <= Math.min(trajet.getStepFin(), this.tempsLimite);
	}

	public EtatVoiture cloneNewEtatVoiture() {
		return new EtatVoiture(this.bonusPrisePassager, this.tempsLimite);
	}

	public int getScore() {
		return score;
	}

	public int getTemps() {
		return temps;
	}

	public Coords getPosition() {
		return position;
	}

	public int getNbBonus() {
		return nbBonus;
	}

	public int getNbCourseReussi() {
		return nbCourseReussi;
	}

	public int getNbTrajet() {
		return nbTrajet;
	}

}
