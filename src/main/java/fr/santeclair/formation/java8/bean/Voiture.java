package fr.santeclair.formation.java8.bean;

import java.util.ArrayList;
import java.util.List;

import fr.santeclair.formation.java8.util.DistanceUtil;

public class Voiture {
	public static final int GAIN_DEFAUT = -10000000;
	private int id;
	private EtatVoiture etat;
	private List<Trajet> trajets;
	private int gainTrajet = GAIN_DEFAUT;

	public Voiture(int id, Integer bonusPrisePassager, Integer tempsLimite) {
		this.id = id;
		this.etat = new EtatVoiture(bonusPrisePassager, tempsLimite);
		this.trajets = new ArrayList<>();
	}

	public boolean hasDispo(Trajet trajet, Integer steps) {
		int distanceParcours = DistanceUtil.getDistance(etat.getPosition(), trajet.getDebut()) + trajet.getDistance();
		return distanceParcours < (steps - etat.getTemps());
	}

	public int estimerGainTrajet(Trajet trajet) {
		if (this.gainTrajet <= GAIN_DEFAUT) {
			this.gainTrajet = this.etat.estimerGainTrajet(trajet);
		}
		return this.gainTrajet;
	}

	public void resetGainTrajet() {
		this.gainTrajet = GAIN_DEFAUT;
	}

	public int getGainTrajet() {
		return this.gainTrajet;
	}

	public int firstStepAvailable() {
		return etat.getTemps();
	}

	public Voiture addTrajet(Trajet trajet) {
		this.trajets.add(trajet);
		this.etat.faireTrajet(trajet);
		return this;
	}

	public EtatVoiture score() {
		EtatVoiture etatscore = this.etat.cloneNewEtatVoiture();
		if (this.trajets.isEmpty()) {
			System.out.println("Il n'y a pas de trajet pour cette voiture !!!");
		} else {
			this.trajets.forEach(t -> etatscore.faireTrajet(t));
			if (this.trajets.size() > Math.min(etatscore.getNbCourseReussi(), etatscore.getNbBonus())) {
				System.out.println("La voiture a effectué " + this.trajets.size() + " trajet(s) dont "
						+ etatscore.getNbCourseReussi() + " avec succès et avec " + etatscore.getNbBonus()
						+ " bonus en prime");
			}
		}
		return etatscore;
	}

	public List<Trajet> getTrajets() {
		return trajets;
	}

	public int getId() {
		return id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Voiture other = (Voiture) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}
}
