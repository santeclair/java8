package fr.santeclair.formation.java8.bean;

import java.util.List;

public class Planning {
	private List<Trajet> trajets;
	private List<Voiture> voitures;

	public Planning(List<Trajet> trajets, List<Voiture> voitures) {
		super();
		this.trajets = trajets;
		this.voitures = voitures;
	}

	public List<Voiture> getVoitures() {
		return voitures;
	}

	public List<Trajet> getTrajets() {
		return trajets;
	}

	@Override
	public String toString() {
		StringBuilder sortie = new StringBuilder();
		for (Voiture voiture : this.voitures) {
			List<Trajet> trajets = voiture.getTrajets();
			sortie.append(trajets.size());
			sortie.append(" ");
			for (Trajet trajet : trajets) {
				sortie.append(trajet.getId());
				sortie.append(" ");
			}
			sortie.append("\n");
		}
		return sortie.toString();
	}
}
