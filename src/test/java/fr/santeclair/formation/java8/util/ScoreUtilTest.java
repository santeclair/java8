package fr.santeclair.formation.java8.util;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import fr.santeclair.formation.java8.bean.Coords;
import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Score;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;

public class ScoreUtilTest {
	@Test
	public void testScoreVoiture1() {

		int bonusPrisePassager = 2;
		int tempsLimite = 10;

		Voiture voiture = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture.addTrajet(new Trajet(0, new Coords(0, 0), new Coords(1, 3), 2, 9));

		int scoreVoiture = voiture.score().getScore();
		Assert.assertEquals(6, scoreVoiture);
	}

	@Test
	public void testScoreVoiture2() {
		int bonusPrisePassager = 2;
		int tempsLimite = 10;

		Voiture voiture = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture.addTrajet(new Trajet(0, new Coords(2, 0), new Coords(2, 2), 0, 9))
				.addTrajet(new Trajet(1, new Coords(1, 2), new Coords(1, 0), 0, 9));

		int scoreVoiture = voiture.score().getScore();
		Assert.assertEquals(4, scoreVoiture);
	}

	@Test
	public void testScorePlanning() {
		int bonusPrisePassager = 2;
		int tempsLimite = 10;

		Trajet t1 = new Trajet(0, new Coords(0, 0), new Coords(1, 3), 2, 9);
		Trajet t2 = new Trajet(1, new Coords(2, 0), new Coords(2, 2), 0, 9);
		Trajet t3 = new Trajet(2, new Coords(1, 2), new Coords(1, 0), 0, 9);

		Voiture voiture1 = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture1.addTrajet(t1);

		Voiture voiture2 = new Voiture(1, bonusPrisePassager, tempsLimite);
		voiture2.addTrajet(t2).addTrajet(t3);

		Planning planning = new Planning(Arrays.asList(t1, t2, t3), Arrays.asList(voiture1, voiture2));

		Score scorePlanning = ScoreUtil.scorePlanning(planning);
		Assert.assertEquals(10, scorePlanning.getScore());
	}

	@Test(expected = IllegalArgumentException.class)
	public void testScorePlanningFailVoitureDouble() {
		int bonusPrisePassager = 2;
		int tempsLimite = 10;

		Trajet t1 = new Trajet(0, new Coords(0, 0), new Coords(1, 3), 2, 9);

		Voiture voiture1 = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture1.addTrajet(t1);

		Planning planning = new Planning(Arrays.asList(t1), Arrays.asList(voiture1, voiture1));

		ScoreUtil.scorePlanning(planning);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testScorePlanningFailTrajetDouble() {
		int bonusPrisePassager = 2;
		int tempsLimite = 10;

		Trajet t1 = new Trajet(0, new Coords(0, 0), new Coords(1, 3), 2, 9);

		Voiture voiture1 = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture1.addTrajet(t1);

		Voiture voiture2 = new Voiture(1, bonusPrisePassager, tempsLimite);
		voiture2.addTrajet(t1);

		Planning planning = new Planning(Arrays.asList(t1), Arrays.asList(voiture1, voiture2));

		ScoreUtil.scorePlanning(planning);
	}

}
