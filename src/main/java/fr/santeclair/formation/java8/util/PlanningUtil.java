package fr.santeclair.formation.java8.util;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import fr.santeclair.formation.java8.bean.Coords;
import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;

public class PlanningUtil {

	private static List<String> loadFile(String fileName) throws IOException {
		List<String> lines = new ArrayList<String>();
		BufferedReader br = null;
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		InputStream stream = classLoader.getResourceAsStream(fileName);
		try {
			br = new BufferedReader(new InputStreamReader(stream));

			String line = null;
			while ((line = br.readLine()) != null) {
				lines.add(line);
			}
			// System.out.println(lines);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (br != null) {
				br.close();
			}
		}
		return lines;
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws FileNotFoundException
	 */
	public static Planning genererPlanningDepuisFichier(String file) throws FileNotFoundException, IOException {
		List<String> lines = new ArrayList<String>();
		lines = loadFile(file);
		String[] conf = lines.get(0).split(" ");
		int rows = Integer.valueOf(conf[0]);
		int columns = Integer.valueOf(conf[1]);
		int vehicule = Integer.valueOf(conf[2]);
		int rides = Integer.valueOf(conf[3]);
		int bonus = Integer.valueOf(conf[4]);
		int steps = Integer.valueOf(conf[5]);
		System.out.println("Fichier: " + file + "\nGrille: " + rows + " x " + columns + "\nNb vÃ©hicules : " + vehicule
				+ "\nNb trajets : " + rides + "\nBonus trajet : " + bonus + "\nTemps total allouÃ© : " + steps);
		List<Trajet> trajets = new ArrayList<Trajet>();
		for (int i = 1; i < rides + 1; i++) {

			String[] line = lines.get(i).split(" ");
			Coords debut = new Coords(Integer.valueOf(line[0]), Integer.valueOf(line[1]));
			Coords fin = new Coords(Integer.valueOf(line[2]), Integer.valueOf(line[3]));
			Trajet trajet = new Trajet(i - 1, debut, fin, Integer.valueOf(line[4]), Integer.valueOf(line[5]));
			trajets.add(trajet);
		}

		Planning planning = genererPlanning(trajets, vehicule, bonus, steps);

		return planning;
	}

	public static Planning genererPlanning(List<Trajet> trajets, Integer nbVoiture, Integer bonus, Integer steps) {
		List<Voiture> voitures = new ArrayList<>();
		for (int i = 0; i < nbVoiture; i++) {
			voitures.add(new Voiture(i, bonus, steps));
		}

		triTrajet(trajets);

		for (Trajet trajet : trajets) {
			voitures = triVoiture(voitures, trajet);
			Voiture voiture = voitures.get(0);
			if (voiture.estimerGainTrajet(trajet) > 0) {
				voiture.addTrajet(trajet);
				trajet.affecterVoiture(voiture);
			}
		}

		return new Planning(trajets, voitures);
	}

	private static List<Trajet> triTrajet(List<Trajet> trajets) {
		Collections.sort(trajets, new Comparator<Trajet>() {
			@Override
			public int compare(Trajet t1, Trajet t2) {
				if (t1.getStepDebut() != t2.getStepDebut()) {
					return t1.getStepDebut() - t2.getStepDebut();
				} else if (t2.getDistance() != t1.getDistance()) {
					return t2.getDistance() - t1.getDistance();
				} else if (t1.getStepFin() != t2.getStepFin()) {
					return t1.getStepFin() - t2.getStepFin();
				} else {
					return t1.getId() - t2.getId();
				}
			}
		});
		return trajets;
	}

	private static List<Voiture> triVoiture(List<Voiture> voitures, final Trajet trajet) {
		for (Voiture voiture : voitures) {
			voiture.resetGainTrajet();
		}
		Collections.sort(voitures, new Comparator<Voiture>() {
			@Override
			public int compare(Voiture v1, Voiture v2) {
				return v2.estimerGainTrajet(trajet) - v1.estimerGainTrajet(trajet);
			}
		});
		return voitures;
	}
}