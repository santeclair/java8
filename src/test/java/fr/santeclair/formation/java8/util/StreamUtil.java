package fr.santeclair.formation.java8.util;

import java.util.Optional;
import java.util.stream.Stream;

public interface StreamUtil {
    public static <T> Stream<T> optionalToStream(Optional<T> optional) {
        if (optional.isPresent()) {
            return Stream.of(optional.get());
        } else {
            return Stream.empty();
        }
    }
}
