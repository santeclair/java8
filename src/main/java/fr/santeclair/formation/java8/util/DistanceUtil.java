package fr.santeclair.formation.java8.util;

import fr.santeclair.formation.java8.bean.Coords;

public class DistanceUtil {
	public static int getDistance(Coords debut, Coords fin) {
		return Math.abs(fin.getRow() - debut.getRow()) + Math.abs(fin.getCol() - debut.getCol());
	}
}
