package fr.santeclair.formation.java8.exercice;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;

import fr.santeclair.formation.java8.bean.Coords;
import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.util.DistanceUtil;
import fr.santeclair.formation.java8.util.PlanningUtil;

public class Exercice4 {
	@Test
	public void goodTestDistance() {
		Coords debut = new Coords(0, 0);
		Coords fin = new Coords(3, 2);
		int distance = DistanceUtil.getDistance(debut, fin);
		Assert.assertEquals(5, distance);
	}

	@Test
	@SuppressWarnings("static-access")
	public void gbadTestDistance() {
		Coords debut = new Coords(0, 0);
		Coords fin = new Coords(3, 2);
		DistanceUtil distanceUtil = new DistanceUtil();
		int distance = distanceUtil.getDistance(debut, fin);
		Assert.assertEquals(5, distance);
	}

	@Test
	public void goodTestGenererPlanningDepuisFichier() throws FileNotFoundException, IOException {
		Planning planning = PlanningUtil.genererPlanningDepuisFichier("a_example.in");
		Assert.assertNotNull(planning);
	}

	@Test
	@SuppressWarnings("static-access")
	public void badTestGenererPlanningDepuisFichier() throws FileNotFoundException, IOException {
		PlanningUtil planningUtil = new PlanningUtil();
		Planning planning = planningUtil.genererPlanningDepuisFichier("a_example.in");
		Assert.assertNotNull(planning);
	}

}
