package fr.santeclair.formation.java8.exercice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.IntSummaryStatistics;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;
import fr.santeclair.formation.java8.util.PlanningUtil;
import fr.santeclair.formation.java8.util.StreamUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercice3 {
	/*
	 * Nouvelle méthodes dans Iterable
	 */
	/**
	 * Réinitialisez le gain de chaque voiture {@link Voiture#resetGainTrajet()}
	 */
	private void exercice01ForEach(List<Voiture> voitures) {
	}

	/*
	 * Nouvelle méthodes dans Collection
	 */
	/**
	 * Supprimez les trajets dont la distance est supérieur à 1000
	 *
	 * @param planning
	 */
	private void exercice02RemoveIf(List<Trajet> trajets) {
	}

	/*
	 * Nouvelle méthodes dans List
	 */
	/**
	 * Remplacez chaque trajet par une version ayant un stepDebut et un stepFin
	 * augmenté de 10.
	 */
	private void exercice03ReplaceAll(List<Trajet> trajets) {
	}

	/*
	 * Stream
	 */
	/**
	 * A l'aide de stream sur les voitures, réinitialisez la liste de trajet de
	 * chaque voiture
	 */
	private void exercice04StreamConsumer(List<Voiture> voitures) {
	}

	/**
	 * A l'aide de stream sur les trajets, affichez dans la console le message
	 * suivant pour chaque trajet :<br>
	 * Trajet <trajet.id> de <trajet.debut> à <trajet.fin>
	 */
	private void exercice05StreamConsumerPeek(List<Trajet> trajets) {
	}

	/**
	 * A l'aide de stream sur les trajets, comptez le nombre de trajet dont la
	 * distance est inférieur à 500
	 */
	private long exercice06StreamPredicate(List<Trajet> trajets) {
		return 0;
	}

	/**
	 * A l'aide de stream sur les trajets, mappez sur les coordonnées de début de
	 * ces trajets puis comptez les coordonnées situé entre le point [50, 200] et
	 * [250, 400]
	 */
	private long exercice07StreamMap(List<Trajet> trajets) {
		return 0;
	}

	/**
	 * A l'aide de stream sur les voitures, compter le nombre de trajet au total
	 * ayant une distance compris entre 500 et 1000 inclus qui leur ont été affecté.
	 */
	private long exercice08StreamFlatMap(List<Voiture> voitures) {
		return 0;
	}

	/**
	 * A l'aide de stream sur les trajets, comptez le nombre de voiture ayant plus
	 * de 20 trajet affecté. Par défaut dans java8, on ne peut pas flatMapper d'un
	 * Optional en Stream, mais heureusement, il y a ici une méthode static pour ça
	 * {@link StreamUtil}
	 */
	private long exercice09StreamFlatMapOptional(List<Trajet> trajets) {
		return 0;
	}

	/**
	 * A l'aide de stream sur les voitures, filtrer les voitures n'ayant pas de
	 * trajet et retournez cette liste sous forme de LinkedList
	 */
	private List<Voiture> exercice10StreamSupplier(List<Voiture> voitures) {
		return null;
	}

	/**
	 * A l'aide de stream sur les voiture, créez une map permettant de récupérer
	 * rapidement une voiture par son identifiant.
	 */
	private Map<Integer, Voiture> exercice11StreamCollectMap(List<Voiture> voitures) {
		return null;
	}

	/**
	 * A l'aide de stream sur les trajets, retournez une liste triée par distance
	 * par ordre décroissant décroissant.
	 */
	private List<Trajet> exercice12StreamSorted(List<Trajet> trajets) {
		return null;
	}

	/**
	 * A l'aide de stream sur les trajets, extrayez des statistique sur la distance
	 * de ces trajets
	 */
	private IntSummaryStatistics exercice13StreamStat(List<Trajet> trajets) {
		return null;
	}

	/**
	 * A l'aide de stream sur les trajets, vérifiez si tous les trajets débutant
	 * avant la 1000e étape aient une distance supérieur à 300
	 */
	private boolean exercice14StreamMatch(List<Trajet> trajets) {
		return false;
	}

	/**
	 * A l'aide de stream sur les voitures, retournez une liste trié par nombre de
	 * trajets décroissant depuis la 2é occurence et limité à 2 occurences
	 */
	private List<Voiture> exercice15StreamSkipAndLimit(List<Voiture> voitures) {
		return null;
	}

	/**
	 * A l'aide de stream sur les voitures, regroupez par nombre de trajets (minimum
	 * 1 trajet), puis extrayez des statistiques sur leur distance parcourue. Cette
	 * map devra être triée par nombre de trajets dans l'ordre décroissant.<br>
	 * Bonus : Si vous voulez et ça peut même vous aider, vous pouvez aussi afficher
	 * dans la console les voitures filtrées avec leur nombre de trajets et leur
	 * distance parcourrue
	 */
	private SortedMap<Integer, IntSummaryStatistics> exercice16Melange(List<Voiture> voitures) {
		return null;
	}

	/*
	 * Vérification des exercices
	 */
	@Test
	public void testExercice01ForEach() {
		Planning planning = getPlanning();

		Assert.assertEquals(true,
				planning.getVoitures().stream().anyMatch(v -> Voiture.GAIN_DEFAUT != v.getGainTrajet()));

		exercice01ForEach(planning.getVoitures());

		Assert.assertEquals(true,
				planning.getVoitures().stream().noneMatch(v -> Voiture.GAIN_DEFAUT != v.getGainTrajet()));
	}

	@Test
	public void testExercice02RemoveIf() {
		Planning planning = getPlanning();

		Assert.assertEquals(300, planning.getTrajets().size());

		exercice02RemoveIf(planning.getTrajets());

		Assert.assertEquals(276, planning.getTrajets().size());
	}

	@Test
	public void testExercice03ReplaceAll() {
		Planning planning = getPlanning();

		List<Trajet> savedTrajets = new ArrayList<>();
		planning.getTrajets().forEach(t -> savedTrajets
				.add(new Trajet(t.getId(), t.getDebut(), t.getFin(), t.getStepDebut(), t.getStepFin())));

		exercice03ReplaceAll(planning.getTrajets());

		for (int i = 0; i < savedTrajets.size(); i++) {
			Assert.assertTrue(checkTrajetModifie(savedTrajets.get(i), planning.getTrajets().get(i)));
		}
	}

	@Test
	public void testExercice04StreamConsumer() {
		Planning planning = getPlanning();

		Assert.assertEquals(true, planning.getVoitures().stream().anyMatch(v -> !v.getTrajets().isEmpty()));

		exercice04StreamConsumer(planning.getVoitures());

		Assert.assertEquals(true, planning.getVoitures().stream().noneMatch(v -> !v.getTrajets().isEmpty()));
	}

	private static boolean checkTrajetModifie(Trajet t1, Trajet t2) {
		return t1.getId() == t2.getId() && t1.getDebut() == t2.getDebut() && t1.getFin() == t2.getFin()
				&& (t1.getStepDebut() + 10) == t2.getStepDebut() && (t1.getStepFin() + 10) == t2.getStepFin();
	}

	@Test
	public void testExercice05StreamConsumerPeek() {
		Planning planning = getPlanning();

		exercice05StreamConsumerPeek(planning.getTrajets());
	}

	@Test
	public void testExercice06StreamPredicate() {
		Planning planning = getPlanning();

		long nbTrajetDistanceInf500 = exercice06StreamPredicate(planning.getTrajets());

		Assert.assertEquals(131, nbTrajetDistanceInf500);
	}

	@Test
	public void testExercice07StreamFunction() {
		Planning planning = getPlanning();

		long nbTrajet50_200to250_400 = exercice07StreamMap(planning.getTrajets());

		Assert.assertEquals(15, nbTrajet50_200to250_400);
	}

	@Test
	public void testExercice08StreamFlatMap() {
		Planning planning = getPlanning();

		long nbTrajetAffecte = exercice08StreamFlatMap(planning.getVoitures());

		Assert.assertEquals(55, nbTrajetAffecte);
	}

	@Test
	public void testExercice09StreamFlatMapOptional() {
		Planning planning = getPlanning();

		long nbVoiturePlus5Trajet = exercice09StreamFlatMapOptional(planning.getTrajets());

		Assert.assertEquals(3, nbVoiturePlus5Trajet);
	}

	@Test
	public void testExercice10StreamSupplier() {
		Planning planning = getPlanning();

		List<Voiture> voitures = exercice10StreamSupplier(planning.getVoitures());

		Assert.assertEquals(96, voitures.size());
		Assert.assertEquals(LinkedList.class, voitures.getClass());
	}

	@Test
	public void testExercice11StreamCollectMap() {
		Planning planning = getPlanning();

		Map<Integer, Voiture> mapVoitureById = exercice11StreamCollectMap(planning.getVoitures());

		planning.getVoitures().forEach(v -> Assert.assertEquals(v, mapVoitureById.get(v.getId())));
	}

	@Test
	public void testExercice12StreamSorted() {
		Planning planning = getPlanning();

		List<Trajet> trajets = exercice12StreamSorted(planning.getTrajets());

		AtomicInteger lastDistance = new AtomicInteger(10000);
		trajets.forEach(t -> Assert.assertTrue(lastDistance.getAndSet(t.getDistance()) >= t.getDistance()));
		Assert.assertEquals(lastDistance.get(), trajets.get(trajets.size() - 1).getDistance());
	}

	@Test
	public void testExercice13StreamStat() {
		Planning planning = getPlanning();

		IntSummaryStatistics stat = exercice13StreamStat(planning.getTrajets());

		Assert.assertEquals(300, stat.getCount());
		Assert.assertEquals(32, stat.getMin());
		Assert.assertEquals(1463, stat.getMax());
		Assert.assertEquals(577.66, stat.getAverage(), 0.01);
		Assert.assertEquals(173_298, stat.getSum());
	}

	@Test
	public void testExercice14StreamMatch() {
		Planning planning = getPlanning();

		boolean isTrajetBefore1000WithDistance300 = exercice14StreamMatch(planning.getTrajets());

		Assert.assertTrue(isTrajetBefore1000WithDistance300);
	}

	@Test
	public void testExercice15StreamSkipAndLimit() {
		Planning planning = getPlanning();

		List<Voiture> voitures = exercice15StreamSkipAndLimit(planning.getVoitures());

		Assert.assertEquals(2, voitures.size());
		Assert.assertEquals(22, voitures.get(0).getTrajets().size());
		Assert.assertEquals(21, voitures.get(1).getTrajets().size());
	}

	@Test
	public void testExercice16Melange() {
		Planning planning = getPlanning();

		Map<Integer, IntSummaryStatistics> map = exercice16Melange(planning.getVoitures());

		Assert.assertEquals(3, map.size());
		Assert.assertNull(map.get(0));
		Iterator<Entry<Integer, IntSummaryStatistics>> iEntrySet = map.entrySet().iterator();
		Entry<Integer, IntSummaryStatistics> entry22 = iEntrySet.next();
		Assert.assertEquals(22, entry22.getKey().intValue());
		Assert.assertEquals(2, entry22.getValue().getCount());
		Assert.assertEquals(17085, entry22.getValue().getMax());
		Entry<Integer, IntSummaryStatistics> entry21 = iEntrySet.next();
		Assert.assertEquals(21, entry21.getKey().intValue());
		Assert.assertEquals(1, entry21.getValue().getCount());
		Assert.assertEquals(17601, entry21.getValue().getMax());
		Entry<Integer, IntSummaryStatistics> entry13 = iEntrySet.next();
		Assert.assertEquals(13, entry13.getKey().intValue());
		Assert.assertEquals(1, entry13.getValue().getCount());
		Assert.assertEquals(9865, entry13.getValue().getMax());
	}

	private Planning getPlanning() {
		try {
			return PlanningUtil.genererPlanningDepuisFichier("b_should_be_easy.in");
		} catch (IOException e) {
			throw new IllegalStateException("Mauvais fichier visiblement", e);
		}

	}
}
