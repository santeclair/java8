package fr.santeclair.formation.java8.bean;

public class Coords {
	private int row;
	private int col;

	public Coords(int row, int col) {
		super();
		this.row = row;
		this.col = col;
	}

	/**
	 * @return the row
	 */
	public int getRow() {
		return row;
	}

	/**
	 * @param row
	 *            the row to set
	 */
	public void setRow(int row) {
		this.row = row;
	}

	/**
	 * @return the col
	 */
	public int getCol() {
		return col;
	}

	/**
	 * @param col
	 *            the col to set
	 */
	public void setCol(int col) {
		this.col = col;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null || !Coords.class.isAssignableFrom(obj.getClass())) {
			return false;
		}
		return this.row == ((Coords) obj).row && this.col == ((Coords) obj).col;
	}

	@Override
	public String toString() {
		return new StringBuilder("[").append(row).append(", ").append(col).append("]").toString();
	}
}
