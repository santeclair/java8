package fr.santeclair.formation.java8;

import java.util.Arrays;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import fr.santeclair.formation.java8.bean.Coords;
import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;

public class TestSortie {

	@Test
	public void testSortie() {
		int bonusPrisePassager = 0;
		int tempsLimite = 0;

		Trajet t1 = new Trajet(0, new Coords(0, 1), new Coords(2, 1), 2, 5);
		Trajet t2 = new Trajet(1, new Coords(2, 1), new Coords(3, 1), 2, 5);
		Trajet t3 = new Trajet(2, new Coords(3, 1), new Coords(4, 1), 2, 5);
		Trajet t4 = new Trajet(3, new Coords(4, 1), new Coords(5, 1), 2, 5);

		Voiture voiture1 = new Voiture(0, bonusPrisePassager, tempsLimite);
		voiture1.addTrajet(t1);

		Voiture voiture2 = new Voiture(1, bonusPrisePassager, tempsLimite);
		voiture2.addTrajet(t2).addTrajet(t3);

		Voiture voiture3 = new Voiture(2, bonusPrisePassager, tempsLimite);
		voiture3.addTrajet(t4);

		List<Voiture> voitures = Arrays.asList(voiture1, voiture2, voiture3);
		Planning planning = new Planning(Arrays.asList(t1, t2, t3, t4), voitures);

		String expected = "1 0 \n2 1 2 \n1 3 \n";
		Assert.assertEquals(expected, planning.toString());
	}
}
