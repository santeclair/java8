package fr.santeclair.formation.java8;

import java.io.IOException;

import org.junit.Test;

import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Score;
import fr.santeclair.formation.java8.util.PlanningUtil;
import fr.santeclair.formation.java8.util.ScoreUtil;

public class TestMain {
	@Test
	public void test() throws IOException {
		int score = 0;
		score += testFile("a_example.in");
		score += testFile("b_should_be_easy.in");
		score += testFile("c_no_hurry.in");
		score += testFile("d_metropolis.in");
		score += testFile("e_high_bonus.in");
		System.out.println("Score total : " + score);
	}

	private int testFile(String file) throws IOException {
		Planning planning = PlanningUtil.genererPlanningDepuisFichier(file);
		Score score = ScoreUtil.scorePlanning(planning);
		return score.getScore();
	}
}
