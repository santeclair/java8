package fr.santeclair.formation.java8.exercice;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BiPredicate;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.ToIntFunction;
import java.util.stream.Collectors;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.santeclair.formation.java8.bean.Coords;
import fr.santeclair.formation.java8.bean.Planning;
import fr.santeclair.formation.java8.bean.Trajet;
import fr.santeclair.formation.java8.bean.Voiture;
import fr.santeclair.formation.java8.util.EntryUtil;
import fr.santeclair.formation.java8.util.PlanningUtil;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class Exercice2 {
	/*
	 * Consumer
	 */
	/**
	 * Créez un consumer lambda qui pour une voiture, accumule ses trajets dans
	 * l'objet sumTrajet en paramètre.
	 */
	private Consumer<Voiture> exercice01ConsumerSum(AtomicInteger sumTrajet) {
		return null;
	}

	/**
	 * Créez un bi-consumer lambda qui pour une voiture et un trajet, ajoute le
	 * trajet à la voiture
	 */
	private BiConsumer<Voiture, Trajet> exercice02BiConsumerAddTrajet() {
		return null;
	}

	/*
	 * Supplier
	 */
	/**
	 * Créez un supplier lambda retournant une nouvelle voiture avec un identifiant
	 * de 0, un bonus de 5 et un temps max de 200.<br>
	 * <b>Attention :</b> Nécessite d'avoir réussi l'exercice 2
	 */
	private Supplier<Voiture> exercice03SupplierVoiture() {
		return null;
	}

	/**
	 * Créez un supplier lambda retournant l'année en cours.
	 */
	private IntSupplier exercice04SupplierYear() {
		return null;
	}

	/*
	 * Function
	 */
	/**
	 * Créez une fonction lambda qui pour un trajet, retourne son identifiant
	 */
	private Function<Trajet, Integer> exercice05FunctionGetId() {
		return null;
	}

	/**
	 * Créez une fonction lambda qui pour une voiture, retourne son nombre de trajet
	 */
	private ToIntFunction<Voiture> exercice06FunctionNbTrajetVoiture() {
		return null;
	}

	/**
	 * Créez une bi-fonction lambda qui pour une voiture et un trajet, vérifie que
	 * le trajet est rentable pour la voiture
	 * ({@link Voiture#estimerGainTrajet(Trajet)}) et retourne le trajet ou null
	 * s'il n'est pas rentable
	 */
	private BiFunction<Voiture, Trajet, Trajet> exercice07BifunctionResetSiNonRentable() {
		return null;
	}

	/*
	 * Predicate
	 */
	/**
	 * Créez un prédicat lambda qui pour un trajet, vérifie que sa distance est
	 * supérieur à 1200. <br>
	 * <b>Attention :</b> Nécessite d'avoir réussi l'exercice 5
	 */
	private Predicate<Trajet> exercice08Predicate1200() {
		return null;
	}

	/**
	 * Créez un bi-prédicat lambda qui pour une voiture et un trajet, vérifie si le
	 * trajet est affecté à la voiture.
	 */
	private BiPredicate<Voiture, Trajet> exercice09BiPredicateTrajetAffecte() {
		return null;
	}

	/*
	 * Pointeur
	 */
	/**
	 * Créez un pointeur de contructeur d'Arraylist
	 */
	private <T> Supplier<List<T>> exercice10SupplierArrayListPointer() {
		return null;
	}

	/**
	 * Créez un pointeur vers la méthode static de récupération de la valeur d'un
	 * {@link Entry} via la classe {@link EntryUtil}.<br>
	 * <b>Attention :</b> Nécessite d'avoir réussi l'exercice 6
	 */
	private <K, V> Function<Entry<K, V>, V> exercice11FunctionGetValueEntryPointer() {
		return null;
	}

	/**
	 * Créez un pointeur vers la méthode d'instance de récupération de la taille
	 * d'une liste via son interface.
	 *
	 * @return
	 */
	private ToIntFunction<Collection<?>> exercice12FunctionSizeCollectionPointer() {
		return null;
	}

	/**
	 * Créer un pointeur vers la méthode d'instance de récupération de la distance
	 * via son implémentation <br>
	 * <b>Attention :</b> Nécessite d'avoir réussi les exercices 5, 10, 11 et 12
	 */
	private Function<Trajet, Integer> exercice13FunctionDistancePointer() {
		return null;
	}

	/**
	 * Créez un pointeur vers la méthode accumulant dans l'objet passé en paramètre
	 */
	private IntConsumer exercice14ConsumerSumPointer(AtomicInteger sumTrajet) {
		return null;
	}

	/*
	 * Vérification des exercices
	 */
	@Test
	public void testExercice01ConsumerSum() {
		AtomicInteger sumTrajet = new AtomicInteger();

		Planning planning = getPlanning();
		planning.getVoitures().forEach(exercice01ConsumerSum(sumTrajet));
		Assert.assertEquals(
				"Vérifier que vous accumulez bien dans le paramètre sumTrajet LES trajets de chaque voiture", 78,
				sumTrajet.get());
	}

	@Test
	public void testExercice02BiConsumerAddTrajet() {
		Map<Voiture, Trajet> mapVoiruteTrajet = new HashMap<>();
		mapVoiruteTrajet.put(new Voiture(0, 5, 10), new Trajet(0, new Coords(2, 10), new Coords(3, 7), 10, 15));
		mapVoiruteTrajet.put(new Voiture(1, 6, 11), new Trajet(1, new Coords(6, 4), new Coords(59, 3), 25, 80));
		mapVoiruteTrajet.put(new Voiture(2, 7, 12), new Trajet(2, new Coords(5, 20), new Coords(2, 6), 140, 158));

		mapVoiruteTrajet.forEach(exercice02BiConsumerAddTrajet());
		mapVoiruteTrajet.forEach((v, t) -> Assert.assertEquals("Vérifiez que vous ajoutez bien le trajet à la voiture",
				t.getFin(), v.score().getPosition()));
	}

	@Test
	public void testExercice03SupplierVoiture() {
		List<Trajet> trajets = Arrays.asList(new Trajet(0, new Coords(2, 10), new Coords(3, 7), 10, 15),
				new Trajet(1, new Coords(6, 4), new Coords(59, 3), 25, 80),
				new Trajet(2, new Coords(5, 20), new Coords(2, 6), 140, 158),
				new Trajet(3, new Coords(3, 6), new Coords(3, 7), 159, 160));

		Voiture voiture = trajets.stream().collect(exercice03SupplierVoiture(), exercice02BiConsumerAddTrajet(),
				(v1, v2) -> System.out.println("do nothing"));
		Assert.assertEquals("Fonctionne quelque soit les paramètres du constructeur", new Coords(3, 7),
				voiture.score().getPosition());
	}

	@Test
	public void testExercice04SupplierYear() {
		Assert.assertEquals("Attention, on est peut être plus en 2018", 2018, exercice04SupplierYear().getAsInt());
	}

	@Test
	public void testExercice05FunctionGetId() {
		Trajet trajet = new Trajet(2, new Coords(5, 20), new Coords(2, 6), 140, 158);
		Integer id = exercice05FunctionGetId().apply(trajet);
		Assert.assertEquals("On veut récupérer l'identifiant du trajet", new Integer(trajet.getId()), id);
	}

	@Test
	public void testExercice06FunctionNbTrajetVoiture() {
		Planning planning = getPlanning();
		int nbTrajetTotal = planning.getVoitures().stream().mapToInt(exercice06FunctionNbTrajetVoiture()).sum();
		Assert.assertEquals(
				"On n'obtient pas le nombre de trajet directement depuis Trajet, il y a un chemin à prendre", 78,
				nbTrajetTotal);
	}

	@Test
	public void testExercice07BifunctionResetSiNonRentable() {
		Map<Voiture, Trajet> mapVoiruteTrajet = new HashMap<>();
		mapVoiruteTrajet.put(new Voiture(0, 5, 100), new Trajet(0, new Coords(2, 10), new Coords(3, 7), 10, 15));
		mapVoiruteTrajet.put(new Voiture(1, 6, 100), new Trajet(1, new Coords(6, 4), new Coords(59, 3), 25, 80));
		mapVoiruteTrajet.put(new Voiture(2, 7, 100), new Trajet(2, new Coords(5, 20), new Coords(2, 6), 140, 158));

		mapVoiruteTrajet.replaceAll(exercice07BifunctionResetSiNonRentable());
		long nbTrajetNull = mapVoiruteTrajet.values().stream().filter(Objects::isNull).count();
		Assert.assertEquals("Non rentable signifie que le gain du trajet est inférieur à 0", 2, nbTrajetNull);
	}

	@Test
	public void testExercice08Predicate1200() {
		Planning planning = getPlanning();
		long nbTrajets = planning.getTrajets().stream().filter(exercice08Predicate1200()).map(exercice05FunctionGetId())
				.count();
		Assert.assertEquals("Strictement supérieur à 1200", 4L, nbTrajets);
	}

	@Test
	public void testExercice09BiPredicateTrajetAffecte() {
		Planning planning = getPlanning();
		Trajet trajet = new Trajet(146, new Coords(481, 80), new Coords(378, 665), 10777, 13072);
		long nbTrajets = planning.getVoitures().stream()
				.filter(v -> exercice09BiPredicateTrajetAffecte().test(v, trajet)).count();
		Assert.assertEquals("Le trajet ne peut être affecté qu'à une seule voiture", 1L, nbTrajets);
	}

	@Test
	public void testExercice10SupplierArrayListPointer() {
		Assert.assertEquals("Le type ArrayList est attendu", ArrayList.class,
				exercice10SupplierArrayListPointer().get().getClass());
	}

	@Test
	public void testExercice11FunctionGetValueEntryPointer() {
		List<Trajet> trajets = Collections
				.singletonMap(1,
						Arrays.asList(new Trajet(0, new Coords(2, 10), new Coords(3, 7), 10, 15),
								new Trajet(1, new Coords(6, 4), new Coords(59, 3), 25, 80),
								new Trajet(2, new Coords(5, 20), new Coords(2, 6), 140, 158)))
				.entrySet().stream().map(exercice11FunctionGetValueEntryPointer()).findFirst().get();

		Assert.assertEquals("Il n'y a qu'une seule méthode dans EntryUtil, on en peut pas la louper", 3,
				trajets.size());
	}

	@Test
	public void testExercice12FunctionSizeCollectionPointer() {
		List<List<Integer>> listListInteger = Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(1, 2, 3, 4, 5),
				Arrays.asList(1, 2, 3, 4));
		List<Integer> shortList = listListInteger.stream()
				.sorted(Comparator.comparingInt(exercice12FunctionSizeCollectionPointer()).reversed()).findFirst()
				.get();
		Assert.assertEquals("On souhaite trier les listes par taille", Arrays.asList(1, 2, 3, 4, 5), shortList);
	}

	@Test
	public void testExercice13FunctionDistancePointer() {
		Comparator<Entry<Integer, List<Trajet>>> byNbTrajet = Comparator.comparing(
				exercice11FunctionGetValueEntryPointer(),
				Comparator.comparingInt(exercice12FunctionSizeCollectionPointer()));

		Planning planning = getPlanning();
		Entry<Integer, List<Trajet>> maxDistance = planning.getTrajets().stream()
				.collect(Collectors.groupingBy(exercice13FunctionDistancePointer())).entrySet().stream()
				.sorted(byNbTrajet.reversed()).findFirst().get();
		List<Integer> idsTrajets = maxDistance.getValue().stream().map(exercice05FunctionGetId()).sorted()
				.collect(exercice10SupplierArrayListPointer(), (l, id) -> l.add(id), (l1, l2) -> l1.addAll(l2));
		Assert.assertEquals("On a besoin de la distance d'un trajet", Arrays.asList(83, 208, 276), idsTrajets);
	}

	@Test
	public void testExercice14ConsumerSumPointer() {
		AtomicInteger sumTrajet = new AtomicInteger();

		Planning planning = getPlanning();
		planning.getVoitures().stream().mapToInt(exercice06FunctionNbTrajetVoiture())
				.forEach(exercice14ConsumerSumPointer(sumTrajet));
		Assert.assertEquals("Assurez vous d'avoir pointer la méthode depuis le paramètre", 78, sumTrajet.get());
	}

	private Planning getPlanning() {
		try {
			return PlanningUtil.genererPlanningDepuisFichier("b_should_be_easy.in");
		} catch (IOException e) {
			throw new IllegalStateException("Mauvais fichier visiblement", e);
		}

	}
}
